### PostgreSgl database
You can use this docker command to set up database:

docker run --name dbCities -e POSTGRES_PASSWORD=postgres -e PGPORT=5432 -p 5432:5432 -d postgres:9.6.10

### api-docs
localhost:8080/api-docs

