package cz.example.cities.domain.comparable;

import cz.example.cities.domain.City;

import java.util.Comparator;

public class CityFavoriteComparator implements Comparator<City> {

    @Override
    public int compare(City firstCity, City secondCity) {
        return Integer.compare(firstCity.getUsers().size(), secondCity.getUsers().size());
    }
}
