package cz.example.cities.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class City{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cityId;
    @Column(unique = true)
    private String name;
    private String description;
    private Long population;
    private LocalDateTime creationDateTime = LocalDateTime.now();

    @JsonIgnore
    @ManyToMany(mappedBy="favoriteCities")
    private Set<Users> users = new HashSet<>();
}
