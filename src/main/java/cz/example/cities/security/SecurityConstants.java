package cz.example.cities.security;

public class SecurityConstants {
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String SIGN_IN_URL = "/login";
    public static final String KEY = "SecretKeyToGenJWTs";
    public static final String HEADER_NAME = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final Long EXPIRATION_TIME = 1000L*60*30;
}
