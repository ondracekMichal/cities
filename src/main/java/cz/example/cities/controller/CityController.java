package cz.example.cities.controller;

import cz.example.cities.domain.City;
import cz.example.cities.service.CityService;
import cz.example.cities.service.dto.CityDTO;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cities")
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping(value = "/all")
    public List<City> getAllCities() {
        return cityService.getAllCities();
    }

    @PostMapping
    public City createCity(@RequestBody CityDTO city) {
        return cityService.createCity(city);
    }

    @PostMapping(value = "favorite/{cityId}")
    public void addToFavoriteCity(@PathVariable Long cityId, Authentication authentication) {
        cityService.addToFavoriteCities(cityId, authentication.getName());
    }

    @DeleteMapping(value = "favorite/{cityId}")
    public void deleteFromFavoriteCity(@PathVariable Long cityId, Authentication authentication) {
        cityService.deleteFromFavoriteCities(cityId, authentication.getName());
    }
}
