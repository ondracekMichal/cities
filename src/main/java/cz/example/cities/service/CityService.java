package cz.example.cities.service;

import cz.example.cities.domain.City;
import cz.example.cities.domain.Users;
import cz.example.cities.domain.comparable.CityFavoriteComparator;
import cz.example.cities.repository.CityRepository;
import cz.example.cities.repository.UserRepository;
import cz.example.cities.service.dto.CityDTO;
import lombok.Synchronized;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class CityService {

    private final CityRepository cityRepository;
    private final UserRepository userRepository;


    public CityService(CityRepository cityRepository, UserRepository userRepository) {
        this.cityRepository = cityRepository;
        this.userRepository = userRepository;
    }

    public City createCity(CityDTO city) {
        City newCity = new City();
        newCity.setDescription(city.getDescription());
        newCity.setPopulation(city.getPopulation());
        newCity.setName(city.getName().toLowerCase().trim());

        return cityRepository.save(newCity);
    }

    public List<City> getAllCities() {
        Comparator<City> byFavorites = Comparator.comparing(city -> city.getUsers().size());
        List<City> cities = cityRepository.findAllByOrderByCreationDateTime();
        cities.sort(byFavorites);
        return cities;
    }

    public void addToFavoriteCities(Long cityId, String userName) {
        Users currentUser = userRepository.findByUsername(userName);
        City city = cityRepository.getOne(cityId);

        currentUser.getFavoriteCities().add(city);
        userRepository.save(currentUser);
    }

    public void deleteFromFavoriteCities(Long cityId, String userName) {
        Users currentUser = userRepository.findByUsername(userName);
        City city = cityRepository.getOne(cityId);

        currentUser.getFavoriteCities().remove(city);
        userRepository.save(currentUser);
    }
}
