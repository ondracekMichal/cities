package cz.example.cities.service.dto;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class LoginDTO {
    @Email
    private String username;
    private String password;
}
