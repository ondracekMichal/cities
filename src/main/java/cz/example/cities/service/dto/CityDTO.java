package cz.example.cities.service.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class CityDTO {
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @Min(value = 0)
    private Long population;
}
