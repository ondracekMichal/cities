package cz.example.cities.service;

import cz.example.cities.domain.Users;
import cz.example.cities.repository.UserRepository;
import cz.example.cities.service.dto.LoginDTO;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void signUp(@Valid LoginDTO loginDTO) {
        Users user = new Users();
        user.setUsername(loginDTO.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(loginDTO.getPassword()));
        userRepository.save(user);
    }
}
